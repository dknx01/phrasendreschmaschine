loader = function(event) {
    let part = $(event).parentsUntil(".col.l4").parent().attr("id");
    let currentWord = $('#'+part).find("span").first().text();
    $.ajax({
        url: '/getWord/' + part + '/' +  currentWord,
        part: part,
        success: function (data) {
            $('#'+this.part).find("span").first().text(data);
        }
    });
};

wordToggle = function (event) {
    let id = $(event).attr('id');
    let entryId = id.substr(0, (id.indexOf('_')));
    $.ajax({
        url: '/api/words/' + entryId,
        method: 'patch',
        success: function (data) {
            let proofing = $('#' + data.id + '_proofing');
            let proofed = $('#' + data.id + '_proofed');
            let proofedAt = $('#' + data.id + '_proofedAt');
            if (data.proofed) {
                proofing.removeClass('not-aproofed');
                proofing.addClass('aproofed');
                proofed.text('Ja');
                proofedAt.text(data.proofedAt);
            } else {
                proofing.removeClass('aproofed');
                proofing.addClass('not-aproofed');
                proofed.text('Nein');
                proofedAt.text('');
            }
        },
        error: function (error) {
            alert('Es gab ein Problem beim Ändern.');
            console.debug(error);
        }
    });
};

wordAproofed = function (event) {
    let id = $(event).attr('id');
    let entryId = id.substr(0, (id.indexOf('_')));
    $.ajax({
        url: '/api/words/' + entryId,
        method: 'put',
        success: function (data) {
            $($('#'+ data.id + '_proofing').parent().parent()).remove();
            let uploadTable = $('#uploads');
            let newRow = '<tr>'+
                '<td id="' + data.id + '_word">' + data.word + '</td>' +
                '<td id="' + data.id + '_type">' + data.type + '</td>' +
                '<td id="' + data.id + '_proofed">nein</td>' +
                '<td id="' + data.id + '_proofedAt">-</td>' +
                '<td id="' + data.id + '_createdAt">' + data.createdAt + '</td>' +
                '<td class="center-align ">' +
                '<span id="' + data.id + ' _proofing" class="btn-floating not-aproofed" onclick="wordToggle(this)"></span>' +
                '</td>' +
                '</tr>';
            uploadTable.append(newRow);
        },
        error: function (error) {
            alert('Es gab ein Problem beim Ändern.')
            console.debug(error);
        }
    });
};

wordTypeConfig = {
    'id': '',
    'type': '',
    'element': null
};

wordType = function () {
    let loaderHtml = '<div class="progress"><div class="indeterminate"></div></div>';
    let type = wordTypeConfig.type;
    let entryId = wordTypeConfig.id;
    $($($("#"+wordTypeConfig.id+"_dropdown")[0]).parent()).append(loaderHtml);
    $($($("#"+wordTypeConfig.id+"_dropdown")[0])).addClass('disabled');
    $.ajax({
        url: '/api/words/' + entryId + '/types/' + type,
        method: 'patch',
        success: function () {
            $($($("#"+wordTypeConfig.id+"_dropdown")[0]).parent()).find('div.progress').remove();
            $($($("#"+wordTypeConfig.id+"_dropdown")[0])).removeClass('disabled');
            let entry = $('#'+wordTypeConfig.id+'_type');
            entry.text(type.charAt(0).toUpperCase() + type.slice(1));
        },
        error: function (error) {
            $($($("#"+wordTypeConfig.id+"_dropdown")[0]).parent()).find('div.progress').remove();
            $($($("#"+wordTypeConfig.id+"_dropdown")[0])).removeClass('disabled');
            alert('Es gab ein Problem beim Ändern.')
            console.debug(error);
        }
    });
};