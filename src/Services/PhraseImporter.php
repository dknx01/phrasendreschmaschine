<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.03.18
 */

namespace Partei\Phrasendreschmaschine\Services;

use Partei\Phrasendreschmaschine\DTO\Cache;
use Partei\Phrasendreschmaschine\DTO\ChainCache;
use Partei\Phrasendreschmaschine\DTO\Phrase;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Yaml\Yaml;

class PhraseImporter
{
    /** @var string */
    private $filePath;

    /** @var ChainCache */
    private $cache;

    /** @var string */
    private $uploadFile;

    /**
     * @param string $filePath
     * @param ChainCache $cache
     * @param string $uploadFile
     */
    public function __construct($filePath, ChainCache $cache, $uploadFile)
    {
        $this->filePath = $filePath;
        $this->cache = $cache;
        $this->uploadFile = $uploadFile;
    }

    /**
     * @param Banner $wordBanner
     * @return Phrase
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getPhrase(Banner $wordBanner): Phrase
    {
        $verb = $this->getVerb($wordBanner);
        $subject = $this->getSubject($wordBanner);
        $adjective = $this->getAdjective($wordBanner);

        return new Phrase($subject, $verb, $adjective);
    }

    /**
     * @param string $part
     * @param Banner $banner
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getPart($part, Banner $banner): string
    {
        return $this->getPhrasePart($part, $banner);
    }

    /**
     * @param string $part
     * @return array
     * @throws \RuntimeException
     */
    private function import($part): array
    {
        if (!is_readable($this->filePath)) {
            throw new \RuntimeException('Phrase file is not readable or does not exists');
        }
        $yaml = Yaml::parse(file_get_contents($this->filePath))[$part];

        return $this->importUploadFile($part, $yaml);
    }

    /**
     * @param string $key
     * @return \Symfony\Component\Cache\CacheItem
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getCacheItemByKey($key): CacheItem
    {
        $item = $this->cache->getItem($key);
        if (!$item->isHit()) {
            $item->set(new Cache($key, $this->import($key)));
            $this->cache->save($item);
            $this->cache->commit();
        }

        return $item;
    }

    /**
     * @param Banner $banner
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getVerb(Banner $banner): string
    {
        return $this->getPhrasePart('verbs', $banner);
    }

    /**
     * @param string $part
     * @param Banner $banner
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getPhrasePart($part, Banner $banner): string
    {
        /** @var Cache $words */
        $words = $this->getCacheItemByKey($part)->get();
        $word = null;
        while ((!$banner->isWordAllowed($word))
            && (!$banner->isPhrasedAllowedByWordPart($word, $part))
        ) {
            $word = $words->getRandom();
        }

        return $word;
    }

    /**
     * @param Banner $banner
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getSubject(Banner $banner): string
    {
        return $this->getPhrasePart('subjects', $banner);
    }

    /**
     * @param Banner $banner
     * @return string
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getAdjective(Banner $banner): string
    {
        return $this->getPhrasePart('adjectives', $banner);
    }

    /**
     * @param string $part
     * @param array $yaml
     * @return array
     */
    private function importUploadFile($part, array $yaml): array
    {
        if (file_exists($this->uploadFile) && is_readable($this->uploadFile)) {
            $uploadContent = Yaml::parse(file_get_contents($this->uploadFile));
            if (array_key_exists($part, $uploadContent)) {
                foreach ($uploadContent[$part] as $item) {
                    if ($item['proofed']) {
                        $yaml[] = $item['word'];
                    }
                }
            }
        }
        return $yaml;
    }
}