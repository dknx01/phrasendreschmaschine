<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 07.10.18
 */

namespace Partei\Phrasendreschmaschine\Services;

use League\Flysystem\Filesystem;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;
use Partei\Phrasendreschmaschine\Repository\PhraseRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionManager
{
    private const sessionFile = 'var/cache/file.session';
    private static $sessionKey = 'PhraseGeneratorWordBanner';

    /** @var SessionInterface */
    private $session;

    /** @var Filesystem */
    private $fileSystem;

    /** @var string[] */
    private $fileSessionList;

    /** @var string|null */
    private $currentIp;

    /**
     * SessionManager constructor.
     * @param SessionInterface $session
     * @param Filesystem $fileSystem
     * @param string $fileSessionList
     * @param RequestStack $requestStack
     */
    public function __construct(SessionInterface $session, Filesystem $fileSystem, string $fileSessionList, RequestStack $requestStack)
    {
        $this->session = $session;
        $this->fileSystem = $fileSystem;
        $this->fileSessionList = explode(',', $fileSessionList);
        $request = $requestStack->getCurrentRequest();
        if ($request) {
            $this->currentIp = $request->getClientIp() ?: null;
        }
    }

    /**
     * @param Banner $banner
     * @return Banner
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getSession(Banner $banner): Banner
    {
        if ($this->currentIp !== null && \in_array($this->currentIp, $this->fileSessionList)) {
            return $this->getFileSession($banner);
        }
        return $this->session->get(self::$sessionKey, new Banner());

    }

    /**
     * @param Banner $banner
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function setSession(Banner $banner)
    {
        if ($this->currentIp !== null && \in_array($this->currentIp, $this->fileSessionList)) {
            $this->setFileSession($banner);
        } else {
            $this->session->set(self::$sessionKey, $banner);
        }
    }

    /**
     * @param Banner $banner
     * @return Banner
     * @throws \League\Flysystem\FileNotFoundException
     */
    private function getFileSession(Banner $banner): Banner
    {
        if ($this->fileSystem->has(self::sessionFile)) {
            return \unserialize($this->fileSystem->read(self::sessionFile), [Banner::class]);
        }
        return $banner;
    }

    /**
     * @param Banner $banner
     */
    private function setFileSession(Banner $banner)
    {
        $this->fileSystem->put(self::sessionFile, \serialize($banner));
    }
}