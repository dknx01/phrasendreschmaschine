<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.03.18
 */

namespace Partei\Phrasendreschmaschine\Services;

use Partei\Phrasendreschmaschine\DTO\Phrase;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;

class PhraseGenerator
{
    /** @var string */
    private static $sessionKey = 'PhraseGeneratorWordBanner';

    /** @var PhraseImporter */
    private $phraseImporter;

    /** @var SessionManager */
    private $session;

    /**
     * @param PhraseImporter $phraseImporter
     * @param SessionManager $session
     */
    public function __construct(PhraseImporter $phraseImporter, SessionManager $session)
    {
        $this->phraseImporter = $phraseImporter;
        $this->session = $session;
    }

    /**
     * @return Phrase
     * @throws \League\Flysystem\FileNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function generate(): Phrase
    {
        $phrase = new Phrase('', '', '');

        $banner = $this->getBanner();

        while (!$banner->isPhrasedAllowed($phrase->get())) {
            $phrase = $this->phraseImporter->getPhrase($banner);
        }

        $banner->setCompletePhrase($phrase);

        $this->session->setSession($banner);

        return $phrase;
    }

    /**
     * @return Banner
     * @throws \League\Flysystem\FileNotFoundException
     */
    private function getBanner(): Banner
    {
        return $this->session->getSession(new Banner());
    }

    /**
     * @param string $part
     * @return string
     * @throws \League\Flysystem\FileNotFoundException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getPhrasePart($part): string
    {
        if ($part[\strlen($part)-1] !== 's') {
            $part .= 's';
        }
        return $this->phraseImporter->getPart($part, $this->getBanner());
    }
}