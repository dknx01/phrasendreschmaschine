<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 02.07.18
 */

namespace Partei\Phrasendreschmaschine\Services;

use League\Flysystem\Filesystem;
use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Repository\PhraseRepository;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;

class CustomWordManager
{
    /** @var PhraseRepository */
    private $phraseRepo;

    /** @var Swift_Mailer */
    private $mailer;

    /** @var Twig_Environment */
    private $twig;
    /** @var Filesystem */
    private $flysystem;
    /** @var string */
    private $uploadFile;
    /** @var string */
    private $newWordFile;
    /** @var array */
    private $emailConfig;

    public const UPLOADS_YML = 'uploads.yml';
    public const NEW_WORDS_YML = 'newWords.yml';

    /**
     * CustomWordManager constructor.
     * @param PhraseRepository $phraseRepo
     * @param Swift_Mailer $mailer
     * @param Twig_Environment $twig
     * @param Filesystem $filesystem
     * @param string $uploadFile
     * @param array $emailConfig
     * @param string $newWordFile
     */
    public function __construct(
        PhraseRepository $phraseRepo,
        Swift_Mailer $mailer,
        Twig_Environment $twig,
        Filesystem $filesystem,
        $uploadFile,
        array $emailConfig,
        $newWordFile
    ) {
        $this->phraseRepo = $phraseRepo;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->flysystem = $filesystem;
        $this->uploadFile = $uploadFile;
        $this->newWordFile = $newWordFile;
        foreach (explode(';', $emailConfig['receivers']) as $receiver) {
            $receiver = explode(':', $receiver);
            $emailConfig['receivers_transformed'][$receiver[0]] = $receiver[1];
        }
        $this->emailConfig = $emailConfig;
    }

    /**
     * @param Upload $upload
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function addNewWord(Upload $upload):void
    {
        $this->phraseRepo->saveNewWord($upload);
        $this->saveNewWordFileOnFileServer();
        $this->sendUploadEmail($upload);
    }

    /**
     * @param string $id
     * @return array
     */
    public function toggleProofStatusById($id):array
    {
        $word = $this->phraseRepo->toggleProofStatusById($id);
        $this->saveUploadFileOnFileServer();

        return $word;
    }

    private function saveUploadFileOnFileServer(): void
    {
        $content = file_get_contents($this->uploadFile);
        if ($this->flysystem->has(self::UPLOADS_YML)) {
            $oldContent = Yaml::parse($this->flysystem->read(self::UPLOADS_YML));
            $newContent = Yaml::parse($content);
            foreach ($oldContent as $type => $words) {
                foreach ($newContent[$type] as $word) {
                    $oldContent[$type][] = $word;
                }
            }
            $content = Yaml::dump($oldContent);
        }
        $this->flysystem->put(self::UPLOADS_YML, $content);
    }

    private function saveNewWordFileOnFileServer(): void
    {
        $content = file_get_contents($this->newWordFile);
        if ($this->flysystem->has(self::NEW_WORDS_YML)) {
            $oldContent = Yaml::parse($this->flysystem->read(self::NEW_WORDS_YML));
            $newContent = Yaml::parse($content);
            foreach ($oldContent as $type => $words) {
                foreach ($newContent[$type] as $word) {
                    $oldContent[$type][] = $word;
                }
            }
            $content = Yaml::dump($oldContent);
        }
        $this->flysystem->put(self::NEW_WORDS_YML, $content);
    }

    /**
     * @param string $id
     * @param string $type
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function updateWordType(string $id, string $type): void
    {
        $words = $this->phraseRepo->getAllNewWords();

        array_walk($words['words'], function (array &$entry) use ($id, $type) {
                if ($entry['id'] === $id) {
                    $entry['type'] = $type;
                }
            }
        );

        $this->phraseRepo->saveNewWordsContent($words);
        $this->flysystem->put(self::NEW_WORDS_YML, file_get_contents($this->newWordFile));
    }

    /**
     * @param string $id
     * @return array
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function acceptWordById(string $id): array
    {
        $words = $this->phraseRepo->getAllNewWords();
        $wordsToKeep = [];

        $wordsToKeep['words'] = array_filter($words['words'],
            function (array $entry) use ($id) {
                return $entry['id'] !== $id;
            });

        $this->phraseRepo->saveNewWordsContent($wordsToKeep);
        $this->flysystem->put(self::NEW_WORDS_YML, file_get_contents($this->newWordFile));

        $words = array_filter($words['words'],
            function (array $entry) use ($id) {
                return $entry['id'] === $id;
            });
        $words = current($words);

        $this->phraseRepo->saveUpload($words);
        $this->saveUploadFileOnFileServer();

        return $words;
    }

    /**
     * @param Upload $upload
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function sendUploadEmail(Upload $upload):void
    {
        $message = new Swift_Message();
        $message->setSubject('Neues Wort für die Phrasendreschmaschine')
            ->setBody(
                $this->twig->render(
                    'Partei/Phrasendreschmachine/email.html.twig',
                    ['upload' => $upload]
                )
            );
        foreach ($this->emailConfig['receivers_transformed'] as $address => $name) {
            $message->addTo($address, $name);
        }
        $message->addFrom($this->emailConfig['sender']);
        $this->mailer->send($message);
    }
}