<?php

namespace Partei\Phrasendreschmaschine\DTO;

use Swagger\Annotations as SWG;

class PhrasePart
{
    /**
     * @var string
     * @SWG\Property(description="the type of the phrase")
     */
    private $type;

    /**
     * @var string
     * @SWG\Property(description="the word")
     */
    private $word;

    /**
     * @var string[]
     */
    public static $availableTypes = [
        PhraseTypeInterface::TYPE_SUBJECT,
        PhraseTypeInterface::TYPE_VERB,
        PhraseTypeInterface::TYPE_ADJECTIVE
    ];

    /**
     * @param string $type
     * @param string $word
     * @throws \RuntimeException
     */
    public function __construct(string $type, string $word)
    {
        if (!in_array($type, self::$availableTypes)) {
            throw new \RuntimeException('Type "' . $type . '" is not allowed');
        }
        $this->type = $type;
        $this->word = $word;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode([
            'type' => $this->type,
            'word' => $this->word,
        ]);
    }
}