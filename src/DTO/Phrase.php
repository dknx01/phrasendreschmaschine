<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.03.18
 */

namespace Partei\Phrasendreschmaschine\DTO;

use Swagger\Annotations as SWG;

class Phrase
{
    /**
     * @var string
     * @SWG\Property(description="the subject")
     */
    private $subject;

    /**
     * @var string
     * @SWG\Property(description="the verb")
     */
    private $verb;

    /**
     * @var string
     * @SWG\Property(description="the adjective")
     */
    private $adjective;

    /**
     * @var string
     * @SWG\Property(description="the whole phrase")
     */
    private $phrase;

    /**
     * Phrase constructor.
     * @param string $subject
     * @param string $verb
     * @param string $adjective
     */
    public function __construct($subject, $verb, $adjective)
    {
        $this->subject = $subject;
        $this->verb = $verb;
        $this->adjective = $adjective;
        $this->phrase = sprintf('%s %s %s', $this->subject, $this->adjective, $this->verb);
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getVerb(): string
    {
        return $this->verb;
    }

    /**
     * @return string
     */
    public function getAdjective(): string
    {
        return $this->adjective;
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->phrase;
    }

    /**
     * @return string
     */
    public function getPhrase(): string
    {
        return $this->phrase;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->get();
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode([
            'subject' => $this->getSubject(),
            'verb' => $this->getVerb(),
            'adjective' => $this->getAdjective(),
            'phrase' => $this->phrase
        ]);
    }
}