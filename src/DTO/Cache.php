<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.03.18
 */

namespace Partei\Phrasendreschmaschine\DTO;

class Cache
{
    /** @var string */
    private $id;

    /** @var int */
    private $size;

    /** @var string[] */
    private $elements = [];

    /**
     * @param string $id
     * @param string[] $elements
     */
    public function __construct($id, array $elements)
    {
        $this->id = $id;
        $this->setElements($elements);
    }

    /**
     * @return string
     */
    public function getId():string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSize():int
    {
        return $this->size;
    }

    /**
     * @return string[]
     */
    public function getElements():array
    {
        return $this->elements;
    }

    /**
     * @param string[] $elements
     */
    public function setElements(array $elements):void
    {
        $this->elements = $elements;
        $this->size = \count($this->elements);
    }

    /**
     * @param string $value
     */
    public function removeElementByValue($value):void
    {
        if (($key = array_search($value, $this->elements)) !== false) {
            unset($this->elements[$key]);
        }
    }

    /**
     * @param string $element
     */
    public function addElement($element):void
    {
        $this->elements[] = $element;
        $this->size = count($this->elements);
    }

    /**
     * @param int $key
     * @return string
     */
    public function getElementsByKey($key):string
    {
        return $this->elements[$key];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getRandom():string
    {
        $index = random_int(0, $this->size -1 );
        while (!array_key_exists($index, $this->elements)) {
            /** @codeCoverageIgnoreStart */
            $index = random_int(0, $this->size -1 );
            /** @codeCoverageIgnoreEnd */
        }
        return $this->elements[$index];
    }
}