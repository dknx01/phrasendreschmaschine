<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 08.04.18
 */

namespace Partei\Phrasendreschmaschine\DTO\Session;

use Partei\Phrasendreschmaschine\DTO\Phrase;

class Banner
{
    /** @var int */
    private static $maxBanWord= 7;

    /** @var int */
    private static $maxBanPhrase = 49;

    /** @var array */
    private $words = [];

    /** @var array */
    private $phrases = [];

    /** @var string[] */
    private $lastCompletePhrase;

    public function __construct()
    {
        $this->lastCompletePhrase = ['adjective' => '', 'subject'=> '', 'verb' => ''];
    }

    /**
     * @param Phrase $phrase
     */
    public function setCompletePhrase(Phrase $phrase): void
    {
        $this->lastCompletePhrase['adjective'] = $phrase->getAdjective();
        $this->lastCompletePhrase['subject'] = $phrase->getSubject();
        $this->lastCompletePhrase['verb'] = $phrase->getVerb();
    }

    /**
     * @param string $word
     */
    private function addWord($word): void
    {
        $this->words[$word] = self::$maxBanWord;
    }

    /**
     * @param string $phrase
     */
    private function addPhrase($phrase): void
    {
        $this->phrases[$phrase] = self::$maxBanPhrase;
    }

    /**
     * @param string|null $word
     * @return bool
     */
    public function isWordAllowed($word): bool
    {
        if ($word === null) {
            return false;
        }
        if (array_key_exists($word, $this->words)) {
            $this->handleWord($word);
            return false;
        }
        $this->addWord($word);
        return true;
    }

    /**
     * @param string|null $phrase
     * @return bool
     */
    public function isPhrasedAllowed($phrase): bool
    {
        if ($phrase === null || trim($phrase) === '') {
            return false;
        }
        if (array_key_exists($phrase, $this->phrases)) {
            $this->handlePhrase($phrase);
            return false;
        }
        $this->addPhrase($phrase);
        return true;
    }

    /**
     * @param string $word
     */
    private function handleWord($word): void
    {
        $this->decreaseWordBanCounter($word);
        $this->removeUnbannedWord($word);
    }

    /**
     * @param string $phrase
     */
    private function handlePhrase($phrase): void
    {
        $this->decreasePhraseBanCounter($phrase);
        $this->removeUnbannedPhrase($phrase);
    }

    /**
     * @param string $word
     */
    private function decreaseWordBanCounter($word): void
    {
        --$this->words[$word];
    }

    /**
     * @param string $phrase
     */
    private function decreasePhraseBanCounter($phrase): void
    {
        --$this->phrases[$phrase];
    }

    /**
     * @param string $word
     */
    private function removeUnbannedWord($word): void
    {
        if ($this->words[$word] <= 0) {
            unset($this->words[$word]);
        }
    }

    /**
     * @param string $phrase
     */
    private function removeUnbannedPhrase($phrase): void
    {
        if ($this->phrases[$phrase] <= 0) {
            unset($this->phrases[$phrase]);
        }
    }

    /**
     * @param string|null $word
     * @param string $part
     * @return bool
     */
    public function isPhrasedAllowedByWordPart($word, $part): bool
    {
        if ($word === null) {
            return false;
        }

        $subject = $part === 'subjects' ? $word : $this->lastCompletePhrase['subject'];
        $verb = $part === 'verbs' ? $word : $this->lastCompletePhrase['verb'];
        $adjective = $part === 'adjectives' ? $word : $this->lastCompletePhrase['adjective'];

        $phrase = new Phrase($subject, $verb, $adjective);

        return $this->isPhrasedAllowed($phrase->get());
    }
}