<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.06.18
 */

namespace Partei\Phrasendreschmaschine\DTO;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class Upload
{
    /**
     * @var string
     */
    private $type = '';

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $word = '';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }

    /**
     * @param string $word
     */
    public function setWord(string $word): void
    {
        $this->word = $word;
    }

    public function getEntry(): array
    {
        return [
            'id' => Uuid::uuid4()->toString(),
            'word' => $this->word,
            'proofed' => false,
            'proofedAt' => null,
            'type' => $this->type,
            'createdAt' => (new \DateTime())->format('Y-m-d H:i:s')
        ];
    }
}