<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.03.18
 */

namespace Partei\Phrasendreschmaschine\DTO;

use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\ChainAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class ChainCache extends ChainAdapter
{
    /**
     * ChainCache constructor.
     * @param FilesystemAdapter $filesystemAdapter
     * @param ApcuAdapter $apcuCache
     */
    public function __construct(FilesystemAdapter $filesystemAdapter, ApcuAdapter $apcuCache)
    {
        parent::__construct([$apcuCache, $filesystemAdapter]);
    }
}