<?php

namespace Partei\Phrasendreschmaschine\DTO;

interface PhraseTypeInterface
{
    public const TYPE_SUBJECT = 'subject';
    public const TYPE_VERB = 'verb';
    public const TYPE_ADJECTIVE = 'adjective';
}