<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 18.06.18
 */

namespace Partei\Phrasendreschmaschine\Controller;

use Partei\Phrasendreschmaschine\DTO\Cache;
use Partei\Phrasendreschmaschine\DTO\ChainCache;
use Partei\Phrasendreschmaschine\DTO\PhrasePart;
use Partei\Phrasendreschmaschine\DTO\PhraseTypeInterface;
use Partei\Phrasendreschmaschine\Services\CustomWordManager;
use Partei\Phrasendreschmaschine\Services\PhraseGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

class PhraseRestController extends Controller
{
    /** @var PhraseGenerator */
    private $phraseGenerator;

    /** @var CustomWordManager */
    private $wordManager;


    /** @var ChainCache */
    private $cache;

    /**
     * PhraseRestController constructor.
     * @param PhraseGenerator $phraseGenerator
     * @param CustomWordManager $customWordManager
     * @param ChainCache $cache
     */
    public function __construct(
        PhraseGenerator $phraseGenerator,
        CustomWordManager $customWordManager,
        ChainCache $cache
    ) {
        $this->phraseGenerator = $phraseGenerator;
        $this->wordManager = $customWordManager;
        $this->cache = $cache;
    }

    /**
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="Get a complete phrase",
     *     @Model(type=Partei\Phrasendreschmaschine\DTO\Phrase::class)
     * )
     */
    public function getPhraseAction(): Response
    {
        $phrase = $this->phraseGenerator->generate();
        return new Response($phrase->toJson());
    }

    /**
     * @param string $oldSubject
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="Get a new subject with preventing it's not the old/given one",
     *     @Model(type=Partei\Phrasendreschmaschine\DTO\PhrasePart::class)
     * )
     * @SWG\Parameter(
     *     name="oldSubject",
     *     type="string",
     *     description="the old subject that should not be generated again",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function getSubjectAction($oldSubject): Response
    {
        $phrase = $oldSubject;
        while ($phrase === $oldSubject) {
            $phrase = $this->phraseGenerator->getPhrasePart(PhraseTypeInterface::TYPE_SUBJECT);
        }
        $phrase = new PhrasePart(PhraseTypeInterface::TYPE_SUBJECT, $phrase);

        return new Response($phrase->toJson());
    }

    /**
     * @param string $oldVerb
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="Get a new verb with preventing it's not the old/given one",
     *     @Model(type=Partei\Phrasendreschmaschine\DTO\PhrasePart::class)
     * )
     * @SWG\Parameter(
     *     name="oldVerb",
     *     type="string",
     *     description="the old ver that should not be generated again",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function getVerbAction($oldVerb): Response
    {
        $phrase = $oldVerb;
        while ($phrase === $oldVerb) {
            $phrase = $this->phraseGenerator->getPhrasePart(PhraseTypeInterface::TYPE_VERB);
        }
        $phrase = new PhrasePart(PhraseTypeInterface::TYPE_VERB, $phrase);

        return new Response($phrase->toJson());
    }

    /**
     * @param string $oldAdjective
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="Get a new adjective with preventing it's not the old/given one",
     *     @Model(type=Partei\Phrasendreschmaschine\DTO\PhrasePart::class)
     * )
     * @SWG\Parameter(
     *     name="oldVerb",
     *     type="string",
     *     description="the old ver that should not be generated again",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function getAdjectiveAction($oldAdjective): Response
    {
        $phrase = $oldAdjective;
        while ($phrase === $oldAdjective) {
            $phrase = $this->phraseGenerator->getPhrasePart(PhraseTypeInterface::TYPE_ADJECTIVE);
        }
        $phrase = new PhrasePart(PhraseTypeInterface::TYPE_ADJECTIVE, $phrase);

        return new Response($phrase->toJson());
    }

    /**
     * @param string $id
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="(un)Proof a word"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     type="string",
     *     description="the id of the word",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function patchWordAction($id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Weg, du darfst das nicht!');
        try {
            $word = $this->wordManager->toggleProofStatusById($id);
            $result = json_encode($word);
            $item = $this->cache->getItem($word['type']);

            $cacheDto = $this->getCacheItem($item, $word);

            if ($word['proofed']) {
                $cacheDto->addElement($word['word']);
            } else {
                $cacheDto->removeElementByValue($word['word']);
            }
            $item->set($cacheDto);
            $this->cache->save($item);
        } catch (\Exception $exception) {
            $result = $exception->getMessage();
        }
        return new Response($result);
    }

    /**
     * @param string $id
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="accept a word"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     type="string",
     *     description="the id of the word",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function putWordAction($id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Weg, du darfst das nicht!');
        try {
            $word = $this->wordManager->acceptWordById($id);
            $result = json_encode($word);
        } catch (\Exception $exception) {
            $result = $exception->getMessage();
        }
        return new Response($result);
    }

    /**
     * @param string $id
     * @param string $type
     * @return Response
     * @SWG\Response(
     *     response=200,
     *     description="change the type of a word"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     type="string",
     *     description="the id of the word",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     * @SWG\Parameter(
     *     name="type",
     *     type="string",
     *     description="the new type of the word",
     *     required=true,
     *     in="path",
     *     minLength=1
     * )
     */
    public function patchWordTypeAction($id, $type): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Weg, du darfst das nicht!');
        $result = '';
        try {
            $this->wordManager->updateWordType($id, $type);
        } catch (\Exception $exception) {
            $result = $exception->getMessage();
        }
        return new Response(json_encode($result));
    }

    /**
     * @param CacheItem $item
     * @param array $word
     * @return Cache
     */
    private function getCacheItem(CacheItem $item, array $word): Cache
    {
        /** @var Cache $cacheDto|null */
        $cacheDto = $item->get();
        if (!$cacheDto instanceof Cache) {
            $cacheDto = new Cache($word['type'], [$word['word']]);
        }
        return $cacheDto;
    }
}