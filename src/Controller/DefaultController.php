<?php

namespace Partei\Phrasendreschmaschine\Controller;

use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Form\UploadType;
use Partei\Phrasendreschmaschine\Repository\CustomWordManager;
use Partei\Phrasendreschmaschine\Services\PhraseGenerator;
use Partei\Phrasendreschmaschine\Services\PhraseImporter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param PhraseGenerator $phraseGenerator
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function indexAction(PhraseGenerator $phraseGenerator): Response
    {
        $phrase = $phraseGenerator->generate();
        return $this->render(
            'Partei/Phrasendreschmachine/index2.html.twig',
            ['phrase' => $phrase]
        );
    }

    /**
     * @Route(
     *     "/getWord/{part}/{old}",
     *     name="byWordPart",
     *     requirements={"part": ".+", "old": ".+"},
     *     methods={"GET"}
     *     )
     * @param string $part
     * @param string $old
     * @param PhraseGenerator $phraseGenerator
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getByWord($part, $old, PhraseGenerator $phraseGenerator): Response
    {
        $phrase = $old;
        while ($phrase === $old) {
            $phrase = $phraseGenerator->getPhrasePart($part);
        }
        if ($part === 'adjective') {
            $phrase = ucfirst($phrase);
        }
        return new Response($phrase);
    }
}
