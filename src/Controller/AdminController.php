<?php

namespace Partei\Phrasendreschmaschine\Controller;

use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Form\UploadType;
use Partei\Phrasendreschmaschine\Repository\PhraseRepository;
use Partei\Phrasendreschmaschine\Services\CustomWordManager;
use Partei\Phrasendreschmaschine\Services\PhraseGenerator;
use Partei\Phrasendreschmaschine\Services\PhraseImporter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @Route(
     *     "/parteibuero/einreichen",
     *     name="new_word",
     *     methods={"GET", "POST"}
     *     )
     * @param Request $request
     * @param CustomWordManager $customWordManager
     * @return Response|RedirectResponse
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function upload(Request $request, CustomWordManager $customWordManager): Response
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $upload = $form->getData();
            $customWordManager->addNewWord($upload);
            return $this->redirectToRoute('accept_word');
        }

        return $this->render(
            'Partei/Phrasendreschmachine/upload.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
    /**
     * @Route(
     *     "/parteibuero/uebersicht",
     *     name="accept_word",
     *     methods={"GET"}
     *     )
     * @Route(
     *     "/parteibüro/",
     *     name="office",
     *     methods={"GET"},
     *     options={"utf8": true}
     *     )
     * @param PhraseRepository $phraseRepository
     * @return Response
     */
    public function upload_list(PhraseRepository $phraseRepository): Response
    {
        return $this->render(
            'Partei/Phrasendreschmachine/upload_overview.html.twig',
            [
                'uploads' => $phraseRepository->getAllUploadedWords(),
                'newWords' => $phraseRepository->getAllNewWords()
            ]
        );
    }
}
