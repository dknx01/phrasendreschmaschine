<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.06.18
 */

namespace Partei\Phrasendreschmaschine\Repository;

use League\Flysystem\Filesystem as Flysystem;
use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Services\CustomWordManager;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class PhraseRepository
{
    const PROOFED = 'proofed';

    /** @var Filesystem */
    private $fileSystem;

    /** @var string */
    private $uploadFile;
    /** @var Flysystem */
    private $flyssystem;
    /** @var string */
    private $newWordFile;

    /**
     * PhraseRepository constructor.
     * @param Filesystem $fileSystem
     * @param string $uploadFile
     * @param Flysystem $flysystem
     * @param string$newWordFile
     */

    public function __construct(Filesystem $fileSystem, $uploadFile, Flysystem $flysystem, $newWordFile)
    {
        $this->fileSystem = $fileSystem;
        $this->uploadFile = $uploadFile;
        $this->flyssystem = $flysystem;
        $this->newWordFile = $newWordFile;
    }

    /**
     * @param Upload $upload
     * @throws IOException
     */
    public function saveNewWord(Upload $upload):void
    {
        if ($this->fileSystem->exists($this->newWordFile) && is_readable($this->newWordFile)) {
            if (($yaml = Yaml::parse(file_get_contents($this->newWordFile))) === false) {
                throw new IOException('Cannot read from new word file');
            }
            $yaml['words'][] = $upload->getEntry();
        } else {
            $yaml = [];
            $yaml['words'][] = $upload->getEntry();
        }
        $this->fileSystem->dumpFile($this->newWordFile, Yaml::dump($yaml));
    }

    /**
     * @param array $newWords
     */
    public function saveNewWordsContent(array $newWords): void
    {
        $this->fileSystem->dumpFile($this->newWordFile, Yaml::dump($newWords));
    }

    /**
     * @return array
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getAllUploadedWords(): array
    {
        $uploads = [];
        if (!$this->fileSystem->exists($this->uploadFile)
            && $this->flyssystem->has(CustomWordManager::UPLOADS_YML)) {
            $content = $this->flyssystem->read(CustomWordManager::UPLOADS_YML);
            $this->fileSystem->dumpFile($this->uploadFile, $content);
        }
        if (is_readable($this->uploadFile)) {
            $uploads = Yaml::parse(file_get_contents($this->uploadFile));
        }

        return $uploads;
    }

    /**
     * @return array
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getAllNewWords(): array
    {
        $newWords = [];
        if (!$this->fileSystem->exists($this->newWordFile)) {
            $content = $this->flyssystem->read(CustomWordManager::NEW_WORDS_YML);
            $this->fileSystem->dumpFile($this->newWordFile, $content);
        }
        if (is_readable($this->newWordFile)) {
            $newWords = Yaml::parse(file_get_contents($this->newWordFile));
        }

        return $newWords;
    }

    /**
     * @param string $id
     * @return array
     * @throws \RuntimeException
     */
    public function toggleProofStatusById($id):array
    {
        if ($this->fileSystem->exists($this->uploadFile) && is_readable($this->uploadFile)) {
            $uploads = Yaml::parse(file_get_contents($this->uploadFile));
            /** @var array $words */
            foreach ($uploads as $type => $words) {
                /** @var array $words */
                foreach ($words as $key => $word) {
                    if ($word['id'] === $id) {
                        if ($word['proofed'] === true) {
                            $word = $this->unapproveWord($word);
                        } else {
                            $word = $this->approveWord($word);
                        }
                        $uploads[$type][$key] = $word;
                        $this->fileSystem->dumpFile($this->uploadFile, Yaml::dump($uploads));
                        $word['type'] = $type;
                        return $word;
                    }
                }
            }
        }
        throw new \RuntimeException('cannot update the word');
    }

    /**
     * @param array $word
     * @return array
     */
    private function approveWord(array $word): array
    {
        $word['proofed'] = true;
        $word['proofedAt'] = (new \DateTime())->format('Y-m-d H:i:s');

        return $word;
    }

    /**
     * @param array $word
     * @return array
     */
    private function unapproveWord(array $word): array
    {
        $word['proofed'] = false;
        $word['proofedAt'] = null;

        return $word;
    }

    /**
     * @param array $word
     * @throws IOException
     */
    public function saveUpload(array $word): void
    {
        $type = $word['type'];
        unset($word['type']);
        if ($this->fileSystem->exists($this->uploadFile) && is_readable($this->uploadFile)) {
            if (($yaml = Yaml::parse(file_get_contents($this->uploadFile))) === false) {
                throw new IOException('Cannot read from upload file');
            }
            $yaml[$type][] = $word;
        } else {
            $yaml[$type][] = $word;
        }
        $this->fileSystem->dumpFile($this->uploadFile, Yaml::dump($yaml));
    }
}