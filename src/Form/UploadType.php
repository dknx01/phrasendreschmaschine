<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 24.06.18
 */

namespace Partei\Phrasendreschmaschine\Form;

use Partei\Phrasendreschmaschine\DTO\PhrasePart;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'word',
            TextType::class,
            [
                'label' => 'Das neue Wort',
                'required' => true
            ]
        )
        ->add(
            'save',
            SubmitType::class,
            [
                'label' => 'speichern'
            ]
        );
    }
}