<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\tests\DTO;

use Partei\Phrasendreschmaschine\DTO\Phrase;
use PHPUnit\Framework\TestCase;

class PhraseTest extends TestCase
{
    public function test__construct(): void
    {
        $phrase = new Phrase('Subject', 'verb', 'adjective');
        $this->assertEquals('Subject', $phrase->getSubject());
        $this->assertEquals('verb', $phrase->getVerb());
        $this->assertEquals('adjective', $phrase->getAdjective());
        $this->assertEquals(sprintf
        ('%s %s %s', 'Subject', 'adjective', 'verb'),
            $phrase->getPhrase()
        );
        $this->assertEquals(sprintf
        ('%s %s %s', 'Subject', 'adjective', 'verb'),
            $phrase->get()
        );
        $this->assertEquals(sprintf
        ('%s %s %s', 'Subject', 'adjective', 'verb'),
            $phrase->__toString()
        );
    }

    public function testToJson(): void
    {
        $phrase = new Phrase('Subject', 'verb', 'adjective');
        $expectedJson = json_encode(
            [
                'subject' => 'Subject',
                'verb' => 'verb',
                'adjective' => 'adjective',
                'phrase' => sprintf('%s %s %s', 'Subject', 'adjective', 'verb')
            ]
        );
        $this->assertEquals($expectedJson, $phrase->toJson());
    }
}
