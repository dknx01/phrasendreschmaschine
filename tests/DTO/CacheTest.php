<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\test\DTO;

use Partei\Phrasendreschmaschine\DTO\Cache;
use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    public function testGetSize(): void
    {
        $cache = new Cache('abc123', [
            'Word can be'
        ]);
        $this->assertEquals(1, $cache->getSize());
    }

    public function testRemoveElementByValue(): void
    {
        $elements = [
            'Word can be'
        ];

        $cache = new Cache('abc123', $elements);
        $this->assertEquals($elements, $cache->getElements());

        $cache->removeElementByValue('Word can be');

        $this->assertEquals([], $cache->getElements());
    }

    public function testRemoveElementByValueThatDoesNotExists(): void
    {
        $elements = [
            'Word can be'
        ];
        $cache = new Cache('abc123', $elements);
        $this->assertEquals($elements, $cache->getElements());

        $cache->removeElementByValue('Word not exits');

        $this->assertEquals($elements, $cache->getElements());
    }

    public function test__construct(): void
    {
        $cache = new Cache('abc123', []);
        $this->assertEquals('abc123', $cache->getId());
    }

    public function testAddElement(): void
    {
        $elements = [
            'Word can be'
        ];

        $cache = new Cache('abc123', $elements);
        $this->assertEquals($elements, $cache->getElements());

        $cache->addElement('Another word now');

        $this->assertEquals(
            array_merge($elements, ['Another word now']),
            $cache->getElements()
        );
    }

    public function testSetElements(): void
    {
        $elements = [
            'Word can be'
        ];
        $elements2 = array_merge($elements, ['Another word now']);

        $cache = new Cache('abc123', $elements);
        $this->assertEquals($elements, $cache->getElements());
        $this->assertCount(1, $cache->getElements());

        $cache->setElements($elements2);

        $this->assertCount(2, $cache->getElements());
        $this->assertEquals($elements2,  $cache->getElements());
    }

    public function testGetElementsByKey()
    {
        $elements = [
            'Word can be',
            'Another word now'
        ];

        $cache = new Cache('abc123', $elements);

        $this->assertEquals('Another word now',  $cache->getElementsByKey(1));
    }

    public function testGetRandom(): void
    {
        $elements = [
            'Word can be',
            'Another word now'
        ];

        $cache = new Cache('abc123', $elements);
        $this->assertNotFalse(array_search($cache->getRandom(), $elements, true));
    }
}
