<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\test\DTO;

use Partei\Phrasendreschmaschine\DTO\PhrasePart;
use Partei\Phrasendreschmaschine\DTO\PhraseTypeInterface;
use PHPUnit\Framework\TestCase;

class PhrasePartTest extends TestCase
{
    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type "bla" is not allowed
     */
    public function testConstructorWithFalseType(): void
    {
        $phrasePart = new PhrasePart('bla', '');
    }

    public function test__construct(): void
    {
        $phrasePart = new PhrasePart(PhraseTypeInterface::TYPE_SUBJECT, 'Foo');
        $this->assertEquals(PhraseTypeInterface::TYPE_SUBJECT, $phrasePart->getType());
        $this->assertEquals('Foo', $phrasePart->getWord());
    }

    public function testToJson(): void
    {
        $expected = json_encode([
            'type' => PhraseTypeInterface::TYPE_SUBJECT,
            'word' => 'Word',
        ]);

        $this->assertEquals($expected, (new PhrasePart(PhraseTypeInterface::TYPE_SUBJECT, 'Word'))->toJson());
    }
}
