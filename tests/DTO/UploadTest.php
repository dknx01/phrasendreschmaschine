<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\tests\DTO;

use Partei\Phrasendreschmaschine\DTO\PhraseTypeInterface;
use Partei\Phrasendreschmaschine\DTO\Upload;
use PHPUnit\Framework\TestCase;

class UploadTest extends TestCase
{
    public function testGetEntry(): void
    {
        static $patternDateTime = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/';
        static $patternUuidV4 = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-(?:[0-9a-f]{2}){2}-[0-9a-f]{12}$/i';

        $upload = new Upload();
        $upload->setType(PhraseTypeInterface::TYPE_SUBJECT);
        $upload->setWord('Word');

        $actual = $upload->getEntry();
        $this->assertEquals(36, \strlen($actual['id']));
        $this->assertSame(1, preg_match($patternUuidV4, $actual['id']));
        $this->assertNull($actual['proofedAt']);
        $this->assertFalse($actual['proofed']);
        $this->assertEquals(PhraseTypeInterface::TYPE_SUBJECT, $actual['type']);
        $this->assertEquals('Word', $actual['word']);
        $this->assertSame(1, preg_match($patternDateTime, $actual['createdAt']));
    }

    public function testGetterSetter(): void
    {
        $upload = new Upload();
        $upload->setWord('foo');
        $upload->setType(PhraseTypeInterface::TYPE_SUBJECT);
        $this->assertSame(PhraseTypeInterface::TYPE_SUBJECT, $upload->getType());
        $this->assertSame('foo', $upload->getWord());
    }
}
