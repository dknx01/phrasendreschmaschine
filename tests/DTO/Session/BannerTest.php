<?php

namespace Partei\Phrasendreschmaschine\DTO\Session;

use Partei\Phrasendreschmaschine\DTO\Phrase;
use Partei\Phrasendreschmaschine\DTO\PhraseTypeInterface;
use PHPUnit\Framework\TestCase;

class BannerTest extends TestCase
{
    public function testIsWordAllowedWithoutWord(): void
    {
        $banner = new Banner();
        $this->assertFalse($banner->isWordAllowed(null));
    }

    public function testIsWordAllowedWithNewWord(): void
    {
        $banner = new Banner();
        $this->assertTrue($banner->isWordAllowed('NewWord'));
    }

    public function testIsWordAllowedWithKnownWordAndUnderLimit(): void
    {
        $banner = new Banner();
        $banner->isWordAllowed('NewWord');
        $this->assertFalse($banner->isWordAllowed('NewWord'));
    }

    public function testIsWordAllowedWithKnownWordAndAllowedAgain(): void
    {
        $banner = new Banner();
        $banner->isWordAllowed('NewWord');

        $bannerRefl = new \ReflectionObject($banner);
        $words = $bannerRefl->getProperty('words');
        $words->setAccessible(true);
        $wordsValue = $words->getValue($banner);
        $wordsValue['NewWord'] = 1;
        $words->setValue($banner, $wordsValue);

        $this->assertFalse($banner->isWordAllowed('NewWord'));
        $this->assertTrue($banner->isWordAllowed('NewWord'));
    }

    public function testSetCompletePhrase():void
    {
        $banner = new Banner();
        $phrase = new Phrase('subject', 'verb', 'adjective');
        $banner->setCompletePhrase($phrase);

        $this->assertEquals(
            [
                 'subject' => 'subject',
                 'verb' => 'verb',
                 'adjective' => 'adjective'
            ],
            $this->getCompletePhrase($banner)
        );
    }

    public function testIsPhrasedAllowedWithoutPhrase():void
    {
        $banner = new Banner();
        $this->assertFalse($banner->isPhrasedAllowed(null));
    }

    public function testIsPhrasedAllowedWithEmptyPhrase():void
    {
        $banner = new Banner();
        $this->assertFalse($banner->isPhrasedAllowed(''));
    }

    public function testIsPhrasedAllowedWithNewPhrase():void
    {
        $banner = new Banner();
        $banner->setCompletePhrase(new Phrase('Foo', 'verb', 'adjective'));
        $this->assertTrue($banner->isPhrasedAllowed('Bar verb adjective'));
    }

    public function testIsPhrasedAllowedWithSamePhrase():void
    {
        $banner = new Banner();
        $banner->isPhrasedAllowed('Foo verb adjective');
        $this->assertFalse($banner->isPhrasedAllowed('Foo verb adjective'));
    }

    public function testIsPhrasedAllowedByWordPart():void
    {
        $banner = new Banner();

        $bannerReflection = new \ReflectionObject($banner);
        $phrasesReflection = $bannerReflection->getProperty('phrases');
        $phrasesReflection->setAccessible(true);
        $phrasesReflection->setValue(
            $banner,
            [
                'Foo verb adjective' => 5,
                'Foo  ' => 5,
                '  verb' => 5,
                ' adjective ' => 5,
            ]
        );

        $this->assertFalse($banner->isPhrasedAllowedByWordPart('Foo', PhraseTypeInterface::TYPE_SUBJECT . 's'));
        $this->assertFalse($banner->isPhrasedAllowedByWordPart('verb', PhraseTypeInterface::TYPE_VERB . 's'));
        $this->assertFalse($banner->isPhrasedAllowedByWordPart('adjective', PhraseTypeInterface::TYPE_ADJECTIVE . 's'));
    }

    public function testIsPhrasedAllowedByWordPartWithoutWord():void
    {
        $banner = new Banner();

        $this->assertFalse($banner->isPhrasedAllowedByWordPart(null, PhraseTypeInterface::TYPE_SUBJECT . 's'));
    }

    public function testIsPhrasedAllowedByWordPartWithUnsetPhrase():void
    {
        $banner = new Banner();

        $bannerReflection = new \ReflectionObject($banner);
        $phrasesReflection = $bannerReflection->getProperty('phrases');
        $phrasesReflection->setAccessible(true);
        $phrasesReflection->setValue(
            $banner,
            [
                'Foo  ' => 1,
            ]
        );

        $this->assertFalse($banner->isPhrasedAllowedByWordPart('Foo', PhraseTypeInterface::TYPE_SUBJECT . 's'));
    }

    /**
     * @param Banner $banner
     * @return array
     */
    private function getCompletePhrase(Banner $banner): array
    {
        $bannerRefl = new \ReflectionObject($banner);
        $completePhrase = $bannerRefl->getProperty('lastCompletePhrase');
        $completePhrase->setAccessible(true);

        return $completePhrase->getValue($banner);

    }
}
