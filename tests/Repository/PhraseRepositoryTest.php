<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 16.09.18
 */

namespace Partei\Phrasendreschmaschine\tests\Repository;

use League\Flysystem\FileNotFoundException;
use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Repository\PhraseRepository;
use Partei\Phrasendreschmaschine\Services\CustomWordManager;
use Partei\Phrasendreschmaschine\Tests\Helper\FileHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class PhraseRepositoryTest extends TestCase
{
    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        FileHelper::createTemporaryFiles();
    }

    /**
     * @inheritdoc
     */
    protected function tearDown()
    {
        FileHelper::removeTemporaryFiles();
    }

    public function testGetAllNewWordsWithLocalFileReadable(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(true);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $this->assertEquals(Yaml::parse(file_get_contents(FileHelper::$newWordFile)), $repo->getAllNewWords());
    }

    public function testGetAllNewWordsWithLocalFileReadableIsNotReadable(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('exists')
            ->with(FileHelper::$newWordFile . 'bla')
            ->willReturn(true);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile . 'bla');

        $this->assertEmpty($repo->getAllNewWords());
    }

    public function testGetAllNewWordsWithNoLocalFile(): void
    {
        $newWordsFileContent = file_get_contents(FileHelper::$newWordFile);

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(false);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with($this->equalTo(FileHelper::$newWordFile), $this->equalTo($newWordsFileContent));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);
        $flySystem->expects($this->once())
            ->method('read')
            ->with($this->equalTo(CustomWordManager::NEW_WORDS_YML))
            ->willReturn($newWordsFileContent);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $this->assertEquals(Yaml::parse($newWordsFileContent), $repo->getAllNewWords());
    }

    /**
     * @expectedException League\Flysystem\FileNotFoundException
     * @expectedExceptionMessage File not found at path: newWords.yml
     */
    public function testGetAllNewWordsWithNoLocalFileAndExternalException(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(false);
        $fileSystem->expects($this->never())
            ->method('dumpFile');

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);
        $flySystem->expects($this->once())
            ->method('read')
            ->with($this->equalTo(CustomWordManager::NEW_WORDS_YML))
            ->willThrowException(new FileNotFoundException(CustomWordManager::NEW_WORDS_YML));

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->getAllNewWords();
    }

    public function testSaveUpload(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(true);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(FileHelper::$uploadFile, $this->isType('string'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);
        $repo->saveUpload(
            [
                'id' => '6b43c8e0-dd41-41eb-ad88-358a35032521',
                'word' => 'sharknado',
                'proofed' => false,
                'proofedAt' => null,
                'type' => '',
                'createdAt' => '2018-07-21 20:31:58'
            ]
        );
    }

    public function testSaveUploadLocalFileDoesNotExistsOrIsNotReadable(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(false);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(FileHelper::$uploadFile, $this->isType('string'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);
        $repo->saveUpload(
            [
                'id' => '6b43c8e0-dd41-41eb-ad88-358a35032521',
                'word' => 'sharknado',
                'proofed' => false,
                'proofedAt' => null,
                'type' => '',
                'createdAt' => '2018-07-21 20:31:58'
            ]
        );
    }

    /**
     * @expectedException Symfony\Component\Filesystem\Exception\IOException
     * @expectedExceptionMessage Cannot read from upload file
     */
    public function testSaveUploadLocalFileExistsWithInvalidContent(): void
    {
        file_put_contents(FileHelper::$uploadFile, 'false');

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(true);
        $fileSystem->expects($this->never())
            ->method('dumpFile');

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);
        $repo->saveUpload(
            [
                'id' => '6b43c8e0-dd41-41eb-ad88-358a35032521',
                'word' => 'sharknado',
                'proofed' => false,
                'proofedAt' => null,
                'type' => '',
                'createdAt' => '2018-07-21 20:31:58'
            ]
        );
    }

    public function testToggleProofStatusById()
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(true);
        $fileSystem->expects($this->atLeastOnce())
            ->method('dumpFile')
            ->with(FileHelper::$uploadFile, $this->isType('string'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $word = $repo->toggleProofStatusById('c7c804f5-e5a7-4e6a-af8b-2e2abaf6838b');
        $this->assertFalse($word['proofed']);
        $this->assertNull($word['proofedAt']);

        $uploadContent = file_get_contents(FileHelper::$uploadFile);
        $uploadContent = str_replace(
            ['proofed: true', "proofedAt: \'2018-09-04 12:23:30\'"],
            ['proofed: false', 'proofedAt: null'],
            $uploadContent
        );
        file_put_contents(FileHelper::$uploadFile, $uploadContent);

        $word = $repo->toggleProofStatusById('c7c804f5-e5a7-4e6a-af8b-2e2abaf6838b');
        $this->assertTrue($word['proofed']);
        $this->assertNotNull($word['proofedAt']);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage cannot update the word
     */
    public function testToggleProofStatusByIdWithException(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(false);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->toggleProofStatusById('abc');
    }

    public function testSaveNewWordsContent(): void
    {
        $newWords = [
            [
                'id' => '6b43c8e0-dd41-41eb-ad88-358a35032521',
                'word' => 'sharknado',
                'proofed' => false,
                'proofedAt' => null,
                'type' => '',
                'createdAt' => '2018-07-21 20:31:58'
            ]
        ];
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(
                FileHelper::$newWordFile,
                Yaml::dump($newWords)
            );

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->saveNewWordsContent($newWords);
    }

    public function testGetAllUploadedWords(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(true);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $this->assertEquals(
            Yaml::parse(file_get_contents(FileHelper::$uploadFile)),
            $repo->getAllUploadedWords()
        );
    }

    /**
     * @expectedException \League\Flysystem\FileNotFoundException
     * @expectedExceptionMessage File not found at path: uploads.yml
     */
    public function testGetAllUploadedWordsWithExceptionExternalHasNotFile(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(false);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);
        $flySystem->expects($this->once())
            ->method('has')
            ->with(CustomWordManager::UPLOADS_YML)
            ->willThrowException(new FileNotFoundException(CustomWordManager::UPLOADS_YML));

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->getAllUploadedWords();
    }

    /**
     * @expectedException \League\Flysystem\FileNotFoundException
     * @expectedExceptionMessage File not found at path: uploads.yml
     */
    public function testGetAllUploadedWordsWithExceptionExternalCanNotReadFile(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(false);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);
        $flySystem->expects($this->once())
            ->method('has')
            ->with(CustomWordManager::UPLOADS_YML)
            ->willReturn(true);
        $flySystem->expects($this->once())
            ->method('read')
            ->with(CustomWordManager::UPLOADS_YML)
            ->willThrowException(new FileNotFoundException(CustomWordManager::UPLOADS_YML));

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->getAllUploadedWords();
    }

    public function testGetAllUploadedWordsWitLocalFileNotReadable(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile . 'bla')
            ->willReturn(true);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile . 'bla', $flySystem, FileHelper::$newWordFile);

        $this->assertEmpty($repo->getAllUploadedWords());
    }

    public function testGetAllUploadedWordsFromExternal(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$uploadFile)
            ->willReturn(false);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(FileHelper::$uploadFile, $this->isType('string'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);
        $flySystem->expects($this->once())
            ->method('has')
            ->with(CustomWordManager::UPLOADS_YML)
            ->willReturn(true);
        $flySystem->expects($this->once())
            ->method('read')
            ->with(CustomWordManager::UPLOADS_YML)
            ->willReturn('');

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $this->assertInternalType('array', $repo->getAllUploadedWords());
    }

    public function testSaveNewWordWithLocalFile(): void
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(true);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(FileHelper::$newWordFile, $this->stringContains('Word'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $upload = new Upload();
        $upload->setType('');
        $upload->setWord('Word');

        $repo->saveNewWord($upload);
    }
    public function testSaveNewWordWithoutLocalFile(): void
    {
        $upload = new Upload();
        $upload->setType('');
        $upload->setWord('Word');

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(false);
        $fileSystem->expects($this->once())
            ->method('dumpFile')
            ->with(FileHelper::$newWordFile, $this->stringContains('Word'));

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $repo->saveNewWord($upload);
    }

    /**
     * @expectedException Symfony\Component\Filesystem\Exception\IOException
     * @expectedExceptionMessage Cannot read from new word file
     */
    public function testSaveNewWordWithException(): void
    {
        file_put_contents(FileHelper::$newWordFile, 'false');

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->once())
            ->method('exists')
            ->with(FileHelper::$newWordFile)
            ->willReturn(true);

        $flySystem = $this->createMock(\League\Flysystem\Filesystem::class);

        $repo = new PhraseRepository($fileSystem, FileHelper::$uploadFile, $flySystem, FileHelper::$newWordFile);

        $upload = new Upload();

        $repo->saveNewWord($upload);
    }
}
