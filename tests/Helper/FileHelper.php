<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 16.09.18
 */

namespace Partei\Phrasendreschmaschine\Tests\Helper;

class FileHelper
{
    public static $tempDirectory = __DIR__ . '/tmp';
    public static $wordFile = '';
    public static $uploadFile = '';
    public static $newWordFile = '';

    public static function createTemporaryFiles(): void
    {
        self::$wordFile = self::$tempDirectory . '/words.yml';
        self::$uploadFile = self::$tempDirectory . '/upload.yml';


        if (is_dir(self::$tempDirectory)) {
            self::removeTemporaryFiles();
        }
        mkdir(self::$tempDirectory);
        self::createWordFile();
        self::createUploadFile();
        self::createNewWordsFiles();
    }

    public static function createNewWordsFiles(): void
    {
        self::$newWordFile = self::$tempDirectory . '/newWords.yml';
        if (!file_exists(self::$newWordFile)) {
            file_put_contents(
                self::$newWordFile,
                <<<YAML
words:
    - { id: 6b43c8e0-dd41-41eb-ad88-358a35032521, word: sharknado, proofed: false, proofedAt: null, type: '', createdAt: '2018-07-21 20:31:58' }
YAML
            );
        }
    }

    public static function removeTemporaryFiles(): void
    {
        $directoryIterator = new \RecursiveDirectoryIterator(self::$tempDirectory, \RecursiveDirectoryIterator::SKIP_DOTS);
        foreach (new \RecursiveIteratorIterator($directoryIterator, \RecursiveIteratorIterator::CHILD_FIRST) as $entry) {
            if ($entry->isDir()) {
                rmdir($entry->getRealPath());
            } else {
                unlink($entry->getRealPath());
            }
        }
        rmdir(self::$tempDirectory);
    }

    private static function createUploadFile(): void
    {
        if (!file_exists(self::$uploadFile)) {
            file_put_contents(
                self::$uploadFile,
                <<<YAML
subjects:
    - { id: c7c804f5-e5a7-4e6a-af8b-2e2abaf6838b, word: fooooo, proofed: true, proofedAt: \'2018-09-04 12:23:30\', createdAt: \'2018-07-07 12:18:14\' }
YAML
            );
        }
    }

    private static function createWordFile(): void
    {
        if (!file_exists(self::$wordFile)) {
            file_put_contents(
                self::$wordFile,
                <<<YAML
verbs:
  - 'aufblähen'
subjects:
  - 'Aussagen'
adjectives:
  - 'aktiv'
YAML
            );
        }
    }
}