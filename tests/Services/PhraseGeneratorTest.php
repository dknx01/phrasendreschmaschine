<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\tests\Services;

use Partei\Phrasendreschmaschine\DTO\Phrase;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;
use Partei\Phrasendreschmaschine\Services\PhraseGenerator;
use Partei\Phrasendreschmaschine\Services\PhraseImporter;
use Partei\Phrasendreschmaschine\Services\SessionManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PhraseGeneratorTest extends TestCase
{
    public function testGetPhrasePart(): void
    {
        $sessionMock = $this->createSessionMock();
        $phraseGeneratorReflection = new \ReflectionClass(PhraseGenerator::class);
        $sessionKey = $phraseGeneratorReflection->getStaticProperties()['sessionKey'];

        $sessionMock->expects($this->atLeastOnce())
            ->method('getSession')
            ->with($this->isInstanceOf(Banner::class))
            ->willReturn(new Banner());

        /** @var PhraseImporter|MockObject $phraseImporter */
        $phraseImporter = $this->getMockBuilder(PhraseImporter::class)
            ->disableOriginalConstructor()
            ->setMethods(['getPart'])
            ->getMock();
        $phraseImporter->expects($this->atLeastOnce())
            ->method('getPart')
            ->with($this->equalTo('subjects'), $this->isInstanceOf(Banner::class))
            ->willReturn('Example');

        $phraseGenerator = new PhraseGenerator($phraseImporter, $sessionMock);
        $this->assertEquals('Example', $phraseGenerator->getPhrasePart('subject'));
    }

    public function testGenerate(): void
    {
        $sessionMock = $this->createSessionMock();
        $phraseGeneratorReflection = new \ReflectionClass(PhraseGenerator::class);
        $sessionKey = $phraseGeneratorReflection->getStaticProperties()['sessionKey'];

        $sessionMock->expects($this->atLeastOnce())
            ->method('setSession')
            ->with($this->isInstanceOf(Banner::class));
        $sessionMock->expects($this->atLeastOnce())
            ->method('getSession')
            ->with($this->isInstanceOf(Banner::class))
            ->willReturn(new Banner());

        $expectedPhrase = new Phrase('Example', 'verb', 'adjective');

        /** @var PhraseImporter|MockObject $phraseImporter */
        $phraseImporter = $this->createMock(PhraseImporter::class);
        $phraseImporter->expects($this->once())
            ->method('getPhrase')
            ->with($this->isInstanceOf(Banner::class))
            ->willReturn($expectedPhrase);

        $phraseGenerator = new PhraseGenerator($phraseImporter, $sessionMock);
        $this->assertEquals($expectedPhrase, $phraseGenerator->generate());
    }

    /**
     * @return SessionManager|MockObject
     */
    private function createSessionMock(): SessionManager
    {
        return $this->createMock(SessionManager::class);
    }
}
