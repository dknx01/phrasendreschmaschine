<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 09.09.18
 */

namespace Partei\Phrasendreschmaschine\tests\Services;

use Partei\Phrasendreschmaschine\DTO\Cache;
use Partei\Phrasendreschmaschine\DTO\ChainCache;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;
use Partei\Phrasendreschmaschine\Services\PhraseImporter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\CacheItem;
use Partei\Phrasendreschmaschine\Tests\Helper\FileHelper;

class WordsImporterTest extends TestCase
{
    public function setUp()
    {
        FileHelper::createTemporaryFiles();
    }

    public function tearDown()
    {
        FileHelper::removeTemporaryFiles();
    }

    public function testGetPhrase(): void
    {
        $cacheItemSubjects = new CacheItem();
        $cacheItemSubjects->set(
            new Cache('subjects', ['subject'])
        );
        $cacheItemVerbs = new CacheItem();
        $cacheItemVerbs->set(
            new Cache('verbs', ['verb'])
        );

        $cacheItemAdjectives = new CacheItem();
        $cacheItemAdjectives->set(
            new Cache('adjectives', ['adjective'])
        );
        $cache = $this->createMock(ChainCache::class);
        $cache->expects($this->atLeastOnce())
            ->method('getItem')
            ->willReturnMap(
                [
                    ['verbs', $cacheItemVerbs],
                    ['subjects', $cacheItemSubjects],
                    ['adjectives', $cacheItemAdjectives]
                ]

            );

        $phraseImporter = new PhraseImporter(FileHelper::$wordFile, $cache, FileHelper::$uploadFile);
        $banner = new Banner();
        $phrase = $phraseImporter->getPhrase($banner);
        $this->assertEquals('aufblähen', $phrase->getVerb());
        $this->assertContains($phrase->getSubject(), ['Aussagen', 'fooooo']);
        $this->assertEquals('aktiv', $phrase->getAdjective());
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Phrase file is not readable or does not exists
     */
    public function testGetPhraseWithFileReadException(): void
    {
        $cacheItemSubjects = new CacheItem();
        $cacheItemSubjects->set(
            new Cache('subjects', ['subject'])
        );
        $cacheItemVerbs = new CacheItem();
        $cacheItemVerbs->set(
            new Cache('verbs', ['verb'])
        );

        $cacheItemAdjectives = new CacheItem();
        $cacheItemAdjectives->set(
            new Cache('adjectives', ['adjective'])
        );
        $cache = $this->createMock(ChainCache::class);
        $cache->expects($this->atLeastOnce())
            ->method('getItem')
            ->willReturnMap(
                [
                    ['verbs', $cacheItemVerbs],
                    ['subjects', $cacheItemSubjects],
                    ['adjectives', $cacheItemAdjectives]
                ]

            );

        $phraseImporter = new PhraseImporter(FileHelper::$wordFile . 'foo', $cache, FileHelper::$uploadFile);
        $banner = new Banner();
        $phrase = $phraseImporter->getPhrase($banner);
        $this->assertEquals('aufblähen', $phrase->getVerb());
        $this->assertContains($phrase->getSubject(), ['Aussagen', 'fooooo']);
        $this->assertEquals('aktiv', $phrase->getAdjective());
    }

    public function testGetPart(): void
    {
        $cacheItemSubjects = new CacheItem();
        $cacheItemSubjects->set(
            new Cache('subjects', ['subject'])
        );
        $cacheItemVerbs = new CacheItem();
        $cacheItemVerbs->set(
            new Cache('verbs', ['verb'])
        );

        $cacheItemAdjectives = new CacheItem();
        $cacheItemAdjectives->set(
            new Cache('adjectives', ['adjective'])
        );
        $cache = $this->createMock(ChainCache::class);
        $cache->expects($this->atLeastOnce())
            ->method('getItem')
            ->willReturnMap(
                [
                    ['verbs', $cacheItemVerbs],
                    ['subjects', $cacheItemSubjects],
                    ['adjectives', $cacheItemAdjectives]
                ]

            );

        $phraseImporter = new PhraseImporter(FileHelper::$wordFile, $cache, FileHelper::$uploadFile);
        $banner = new Banner();
        $this->assertEquals('aufblähen', $phraseImporter->getPart('verbs', $banner));
    }
}
