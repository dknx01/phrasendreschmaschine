<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 07.10.18
 */

namespace Partei\Phrasendreschmaschine\tests\Services;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Partei\Phrasendreschmaschine\DTO\Session\Banner;
use Partei\Phrasendreschmaschine\Services\SessionManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionManagerTest extends TestCase
{
    public function testSetSessionToHttpSession(): void
    {
        /** @var MockObject|Request $request */
        $request = $this->createMock(Request::class);

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $banner = new Banner();

        $fileSystem = $this->createMock(Filesystem::class);

        /** @var Session|MockObject $session */
        $session = $this->createMock(Session::class);
        $session->expects($this->atLeastOnce())
            ->method('set')
            ->with($this->isType('string'), $banner);

        $sessionManager = new SessionManager(
            $session,
            $fileSystem,
            '',
            $requestStack
        );

        $sessionManager->setSession($banner);
    }
    public function testSetSessionToFileSession(): void
    {
        /** @var MockObject|Request $request */
        $request = $this->createMock(Request::class);
        $request->expects($this->atLeastOnce())
            ->method('getClientIp')
            ->willReturn('0.0.0.0');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $banner = new Banner();

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('put')
            ->with($this->isType('string'), $this->isType('string'));

        /** @var Session|MockObject $session */
        $session = $this->createMock(Session::class);
        $session->expects($this->never())
            ->method('set');

        $sessionManager = new SessionManager(
            $session,
            $fileSystem,
            '0.0.0.0',
            $requestStack
        );

        $sessionManager->setSession($banner);
    }

    public function testGetSessionFromHttpSession(): void
    {
        /** @var MockObject|Request $request */
        $request = $this->createMock(Request::class);

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $banner = new Banner();

        $fileSystem = $this->createMock(Filesystem::class);

        /** @var Session|MockObject $session */
        $session = $this->createMock(Session::class);
        $session->expects($this->atLeastOnce())
            ->method('get')
            ->with($this->isType('string'), $this->isInstanceOf(Banner::class))
            ->willReturn($banner);

        $sessionManager = new SessionManager(
            $session,
            $fileSystem,
            '',
            $requestStack
        );

        $this->assertEquals($banner, $sessionManager->getSession($banner));
    }

    public function testGetSessionFromFile(): void
    {
        /** @var MockObject|Request $request */
        $request = $this->createMock(Request::class);
        $request->expects($this->atLeastOnce())
            ->method('getClientIp')
            ->willReturn('0.0.0.0');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $banner = new Banner();

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->isType('string'))
            ->willReturn(true);
        $fileSystem->expects($this->atLeastOnce())
            ->method('read')
            ->with($this->isType('string'))
            ->willReturn(\serialize($banner));

        /** @var Session|MockObject $session */
        $session = $this->createMock(Session::class);

        $sessionManager = new SessionManager(
            $session,
            $fileSystem,
            '0.0.0.0',
            $requestStack
        );

        $this->assertEquals($banner, $sessionManager->getSession($banner));
    }

    public function testGetSessionFromFileNotPresent(): void
    {
        /** @var MockObject|Request $request */
        $request = $this->createMock(Request::class);
        $request->expects($this->atLeastOnce())
            ->method('getClientIp')
            ->willReturn('0.0.0.0');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $banner = new Banner();

        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->isType('string'))
            ->willReturn(false);
        $fileSystem->expects($this->never())
            ->method('read');

        /** @var Session|MockObject $session */
        $session = $this->createMock(Session::class);

        $sessionManager = new SessionManager(
            $session,
            $fileSystem,
            '0.0.0.0',
            $requestStack
        );

        $this->assertEquals($banner, $sessionManager->getSession($banner));
    }
}
