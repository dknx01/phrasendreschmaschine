<?php
/**
 * phrasendreschmaschine
 * User: dknx01 <e.witthauer@gmail.com>
 * Date: 16.09.18
 */

namespace Partei\Phrasendreschmaschine\test\Services;

use League\Flysystem\Filesystem;
use Partei\Phrasendreschmaschine\DTO\Upload;
use Partei\Phrasendreschmaschine\Repository\PhraseRepository;
use Partei\Phrasendreschmaschine\Services\CustomWordManager;
use Partei\Phrasendreschmaschine\Tests\Helper\FileHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;

class CustomWordManagerTest extends TestCase
{
    /**
     * @var array
     */
    private $emailConfig = [
        'receivers' => 'receiver@example.test:test',
        'sender' => 'sender@example.test'
    ];

    protected function setUp()
    {
        FileHelper::createTemporaryFiles();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        FileHelper::removeTemporaryFiles();
    }

    public function testToggleProofStatusByIdWithNewUpload(): void
    {
        $wordId = '6b43c8e0-dd41-41eb-ad88-358a35032521';
        $word = $this->createWordEntry($wordId);

        $phraseRepo = $this->createMock(PhraseRepository::class);
        $phraseRepo->expects($this->atLeastOnce())
            ->method('toggleProofStatusById')
            ->with($this->equalTo($wordId))
            ->willReturn($word);
        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $twig = $this->createMock(\Twig_Environment::class);
        $flySystem = $this->createMock(Filesystem::class);
        $flySystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML))
            ->willReturn(false);
        $flySystem->expects($this->atLeastOnce())
            ->method('put')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML), $this->isType('string'));

        $customWordManager = new CustomWordManager(
            $phraseRepo,
            $swiftMailer,
            $twig,
            $flySystem,
            FileHelper::$uploadFile,
            $this->emailConfig,
            FileHelper::$newWordFile
            );

        $this->assertEquals($word, $customWordManager->toggleProofStatusById($wordId));
    }

    public function testToggleProofStatusByIdWithExistingUpload(): void
    {
        $wordId = '6b43c8e0-dd41-41eb-ad88-358a35032521';
        $word = $this->createWordEntry($wordId);

        $phraseRepo = $this->createMock(PhraseRepository::class);
        $phraseRepo->expects($this->atLeastOnce())
            ->method('toggleProofStatusById')
            ->with($this->equalTo($wordId))
            ->willReturn($word);
        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $twig = $this->createMock(\Twig_Environment::class);
        $flySystem = $this->createMock(Filesystem::class);
        $flySystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML))
            ->willReturn(true);
        $flySystem->expects($this->atLeastOnce())
            ->method('read')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML))
            ->willReturn(<<<YAML
subjects:
    - { id: c7c804f5-e5a7-4e6a-af8b-2e2abaf6838c, word: blaaa, proofed: true, proofedAt: '2018-09-04 12:23:30', createdAt: '2018-07-07 12:18:14' }
YAML
);
        $flySystem->expects($this->atLeastOnce())
            ->method('put')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML), $this->isType('string'));

        $customWordManager = new CustomWordManager(
            $phraseRepo,
            $swiftMailer,
            $twig,
            $flySystem,
            FileHelper::$uploadFile,
            $this->emailConfig,
            FileHelper::$newWordFile
            );

        $this->assertEquals($word, $customWordManager->toggleProofStatusById($wordId));
    }

    public function testAcceptWordById(): void
    {
        $wordId = '6b43c8e0-dd41-41eb-ad88-358a35032521';
        $word = $this->createWordEntry($wordId);

        $phraseRepo = $this->createMock(PhraseRepository::class);
        $phraseRepo->expects($this->atLeastOnce())
            ->method('getAllNewWords')
            ->willReturn(Yaml::parse(file_get_contents(FileHelper::$newWordFile)));
        $phraseRepo->expects($this->atLeastOnce())
            ->method('saveNewWordsContent')
            ->with($this->isType('array'));
        $phraseRepo->expects($this->atLeastOnce())
            ->method('saveUpload')
            ->with($this->isType('array'));

        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $twig = $this->createMock(\Twig_Environment::class);

        $flySystem = $this->createMock(Filesystem::class);
        $flySystem->expects($this->atLeastOnce())
            ->method('put')
            ->withConsecutive(
                    [$this->equalTo(CustomWordManager::NEW_WORDS_YML), $this->isType('string')],
                    [$this->equalTo(CustomWordManager::UPLOADS_YML), $this->isType('string')]
            );
        $flySystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->equalTo(CustomWordManager::UPLOADS_YML))
            ->willReturn(false);

        $customWordManager = new CustomWordManager(
            $phraseRepo,
            $swiftMailer,
            $twig,
            $flySystem,
            FileHelper::$uploadFile,
            $this->emailConfig,
            FileHelper::$newWordFile
        );
        $word['word'] = 'sharknado';
        $word['createdAt'] = '2018-07-21 20:31:58';
        $word['type'] = '';

        $this->assertEquals($word, $customWordManager->acceptWordById($wordId));
    }

    public function testAddNewWord(): void
    {
        $upload = new Upload();
        $upload->setWord('sharknado');
        $upload->setType('');

        $phraseRepo = $this->createMock(PhraseRepository::class);
        $phraseRepo->expects($this->atLeastOnce())
            ->method('saveNewWord')
            ->with($this->equalTo($upload));

        $flySystem = $this->createMock(Filesystem::class);
        $flySystem->expects($this->atLeastOnce())
            ->method('has')
            ->with($this->equalTo(CustomWordManager::NEW_WORDS_YML))
            ->willReturn(true);
        $flySystem->expects($this->atLeastOnce())
            ->method('read')
            ->with($this->equalTo(CustomWordManager::NEW_WORDS_YML))
            ->willReturn(<<<YAML
words:
    - { id: 6b43c8e0-dd41-41eb-ad88-358a3503252c, word: foo, proofed: false, proofedAt: null, type: '', createdAt: '2018-07-21 20:31:58' }
YAML
);
        $flySystem->expects($this->atLeastOnce())
            ->method('put')
            ->with($this->equalTo(CustomWordManager::NEW_WORDS_YML), $this->isType('string'));

        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $swiftMailer->expects($this->atLeastOnce())
            ->method('send')
            ->with($this->isInstanceOf(\Swift_Message::class));

        $twig = $this->createMock(\Twig_Environment::class);
        $twig->expects($this->atLeastOnce())
            ->method('render')
            ->with($this->isType('string'), $this->isType('array'))
            ->willReturn('<html>TEST</html>');

        $customWordManager = new CustomWordManager(
            $phraseRepo,
            $swiftMailer,
            $twig,
            $flySystem,
            FileHelper::$uploadFile,
            $this->emailConfig,
            FileHelper::$newWordFile
        );

        $customWordManager->addNewWord($upload);
    }

    public function testUpdateWordType(): void
    {
        $wordId = '6b43c8e0-dd41-41eb-ad88-358a35032521';
        $word = $this->createWordEntry($wordId);
        $wordType = $word['type'];

        $phraseRepo = $this->createMock(PhraseRepository::class);
        $phraseRepo->expects($this->atLeastOnce())
            ->method('getAllNewWords')
            ->willReturn(Yaml::parse(file_get_contents(FileHelper::$newWordFile)));
        $phraseRepo->expects($this->atLeastOnce())
            ->method('saveNewWordsContent')
            ->with($this->isType('array'));

        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $twig = $this->createMock(\Twig_Environment::class);

        $flySystem = $this->createMock(Filesystem::class);
        $flySystem->expects($this->atLeastOnce())
            ->method('put')
            ->with(
                $this->equalTo(CustomWordManager::NEW_WORDS_YML), $this->isType('string')
            );

        $customWordManager = new CustomWordManager(
            $phraseRepo,
            $swiftMailer,
            $twig,
            $flySystem,
            FileHelper::$uploadFile,
            $this->emailConfig,
            FileHelper::$newWordFile
        );
        $word['word'] = 'sharknado';
        $word['createdAt'] = '2018-07-21 20:31:58';
        $word['type'] = '';

        $customWordManager->updateWordType($wordId, $wordType);
    }

    /**
     * @param $wordId
     * @return array
     */
    private function createWordEntry($wordId): array
    {
        $word = [
            'id' => $wordId,
            'word' => 'fooooo',
            'proofed' => false,
            'proofedAt' => null,
            'type' => 'subject',
            'createdAt' => '2018-08-05 20:24:41'
        ];
        return $word;
    }
}
